import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'evento',
    pathMatch: 'full'
  },
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'evento',
        loadChildren: () => import('../evento/evento.module').then(m => m.EventoModule)
      },
      {
        path: 'funcionario',
        loadChildren: () => import('../funcionario/funcionario.module').then(m => m.FuncionarioModule)
      }
    ],
    canActivate: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
