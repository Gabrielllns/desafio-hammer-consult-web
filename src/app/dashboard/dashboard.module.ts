import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard.component';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { ShowErrorsModule } from '../config/components/show-errors/show-errors.module';
import { ConfirmationDialogModule } from '../config/components/confirmation-dialog/confirmation-dialog.module';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    FormsModule,
    CommonModule,
    ShowErrorsModule,
    DashboardRoutingModule,
    ConfirmationDialogModule
  ]
})
export class DashboardModule { }
