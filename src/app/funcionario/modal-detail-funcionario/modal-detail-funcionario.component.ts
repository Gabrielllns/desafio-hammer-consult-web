import { isUndefined } from 'util';
import { Component, Output, EventEmitter } from '@angular/core';

import { Funcionario } from 'src/app/config/interfaces/funcionario';

@Component({
  selector: 'app-modal-detail-funcionario',
  templateUrl: './modal-detail-funcionario.component.html',
  styleUrls: ['./modal-detail-funcionario.component.scss']
})
export class ModalDetailFuncionarioComponent {

  public funcionario: Funcionario = { usuario: { perfil: {} } };

  @Output() onSuccess: EventEmitter<boolean> = new EventEmitter(false);

  /**
   * Construtor do componente.
   */
  constructor() { }

  /**
   * Inicia as configurações iniciais do modal.
   *
   * @param funcionario
   */
  public init(funcionario: Funcionario): void {
    this.funcionario = { usuario: {} };

    if (!isUndefined(funcionario)) {
      this.funcionario = JSON.parse(JSON.stringify(funcionario));
    }
  }

}
