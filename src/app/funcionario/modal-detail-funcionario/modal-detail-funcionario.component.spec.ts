import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetailFuncionarioComponent } from './modal-detail-funcionario.component';

describe('ModalDetailFuncionarioComponent', () => {
  let component: ModalDetailFuncionarioComponent;
  let fixture: ComponentFixture<ModalDetailFuncionarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDetailFuncionarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDetailFuncionarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
