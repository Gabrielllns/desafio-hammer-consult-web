import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NgxSmartModalService } from 'ngx-smart-modal';

import { Constants } from '../../constants';
import { Funcionario } from 'src/app/config/interfaces/funcionario';
import { FuncionarioService } from 'src/app/config/services/funcionario.service';

@Component({
  selector: 'app-list-funcionario',
  templateUrl: './list-funcionario.component.html',
  styleUrls: ['./list-funcionario.component.scss']
})
export class ListFuncionarioComponent {

  public funcionarios: Funcionario[] = [];

  /**
   * Construtor do componente.
   *
   * @param funcionarioService
   * @param toastrService
   * @param ngxUiLoaderService
   * @param ngxSmartModalService
   * @param activatedRoute
   */
  constructor(private funcionarioService: FuncionarioService, private toastrService: ToastrService, private ngxUiLoaderService: NgxUiLoaderService, private ngxSmartModalService: NgxSmartModalService, private activatedRoute: ActivatedRoute) {
    this.funcionarios = this.activatedRoute.snapshot.data['funcionarios'];
  }

  /**
   * Abre o modal de funcionário.
   *
   * @param nomeModal
   * @param funcionario
   */
  public openModalFuncionario(nomeModal: string, funcionario?: Funcionario): void {
    this.ngxSmartModalService.setModalData(funcionario, nomeModal);
    this.ngxSmartModalService.getModal(nomeModal).open();
  }

  /**
   * Retorna todos os funcionários cadastrados.
   */
  public getFuncionarios(): void {
    this.ngxUiLoaderService.start(Constants.TASKID.LIST_FUNCIONARIO);

    this.funcionarioService.getFuncionarios().subscribe((funcionarios: Funcionario[]) => {
      this.funcionarios = funcionarios;
      this.ngxUiLoaderService.stop(Constants.TASKID.LIST_FUNCIONARIO);
    }, (error) => {
      this.toastrService.error(error);
      this.ngxUiLoaderService.stop(Constants.TASKID.LIST_FUNCIONARIO);
    });
  }

}
