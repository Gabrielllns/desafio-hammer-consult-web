import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListFuncionarioComponent } from './list-funcionario/list-funcionario.component';

import { FuncionariosResolve } from '../config/resolves/funcionarios.resolve';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: 'list',
    component: ListFuncionarioComponent,
    resolve: {
      funcionarios: FuncionariosResolve
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FuncionarioRoutingModule { }
