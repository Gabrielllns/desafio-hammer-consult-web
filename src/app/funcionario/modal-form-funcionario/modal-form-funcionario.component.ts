import { isUndefined } from 'util';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Component, Output, EventEmitter } from '@angular/core';

import { Constants } from '../../constants';
import { Perfil } from 'src/app/config/interfaces/perfil';
import { Funcionario } from 'src/app/config/interfaces/funcionario';
import { PerfilService } from 'src/app/config/services/perfil.service';
import { FuncionarioService } from 'src/app/config/services/funcionario.service';

@Component({
  selector: 'app-modal-form-funcionario',
  templateUrl: './modal-form-funcionario.component.html',
  styleUrls: ['./modal-form-funcionario.component.scss']
})
export class ModalFormFuncionarioComponent {

  public refModal: any;
  public perfis: Perfil[] = [];
  public isUpdate: boolean = false;
  public funcionario: Funcionario = { usuario: {} };

  @Output() onSuccess: EventEmitter<boolean> = new EventEmitter(false);

  /**
   * Construtor do componente.
   *
   * @param funcionarioService
   * @param toastrService
   * @param ngxSpinnerService
   * @param perfilService
   * @param ngxSmartModalService
   */
  constructor(private funcionarioService: FuncionarioService, private toastrService: ToastrService, private ngxUiLoaderService: NgxUiLoaderService, private perfilservice: PerfilService, private ngxSmartModalService: NgxSmartModalService) { }

  /**
   * Inicia as configurações iniciais do modal.
   *
   * @param refModal
   */
  public init(refModal: any): void {
    this.refModal = refModal;

    this.getPerfisEmpresa();

    let funcionario = this.refModal.getData();
    if (!isUndefined(funcionario)) {
      this.isUpdate = true;
      this.funcionario = JSON.parse(JSON.stringify(funcionario));
    } else {
      this.funcionario.usuario.id_perfil = undefined;
    }
  }

  /**
   * Retorna todos os perfis vinculados a empresa cadastrados.
   */
  private getPerfisEmpresa(): void {

    this.perfilservice.getPerfisEmpresa().subscribe((perfis: Perfil[]) => {
      this.perfis = perfis;
    }, (error) => {
      this.refModal.close();
      this.toastrService.error(error);
    });
  }

  /**
   * Reseta as dependências do modal.
   *
   * @param ngForm
   */
  public resetDependencies(ngForm: NgForm): void {
    this.ngxSmartModalService.getModal(this.refModal.identifier).removeData();

    this.funcionario = { usuario: {} };
    this.isUpdate = false;
    ngForm.resetForm();
    this.perfis = [];
  }

  /**
   * Salva o registro com base nos dados informados.
   *
   * @param ngForm
   * @param funcionario
   * @param modalFormFinance
   */
  public save(ngForm: NgForm, funcionario: Funcionario, modalFormFinance: any): void {

    if (ngForm.valid) {
      this.ngxUiLoaderService.start(Constants.TASKID.FORM_FUNCIONARIO);

      this.funcionarioService.salvar(funcionario).subscribe(() => {
        this.toastrService.success((this.isUpdate) ? Constants.MSG.DADOS_ALTERADOS : Constants.MSG.CADASTRO_REALIZADO);

        this.ngxUiLoaderService.stop(Constants.TASKID.FORM_FUNCIONARIO);
        modalFormFinance.close();
        this.onSuccess.emit(true);
      }, (error) => {
        this.toastrService.error(error);
        this.ngxUiLoaderService.stop(Constants.TASKID.FORM_FUNCIONARIO);
      });
    }
  }

}
