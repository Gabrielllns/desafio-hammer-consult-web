import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFormFuncionarioComponent } from './modal-form-funcionario.component';

describe('ModalFormFuncionarioComponent', () => {
  let component: ModalFormFuncionarioComponent;
  let fixture: ComponentFixture<ModalFormFuncionarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFormFuncionarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFormFuncionarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
