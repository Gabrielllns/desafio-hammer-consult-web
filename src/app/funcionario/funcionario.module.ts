import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxSmartModalModule } from 'ngx-smart-modal';

import { ListFuncionarioComponent } from './list-funcionario/list-funcionario.component';
import { ModalFormFuncionarioComponent } from './modal-form-funcionario/modal-form-funcionario.component';
import { ModalDetailFuncionarioComponent } from './modal-detail-funcionario/modal-detail-funcionario.component';

import { FuncionarioRoutingModule } from './funcionario-routing.module';
import { ShowErrorsModule } from '../config/components/show-errors/show-errors.module';

@NgModule({
  declarations: [
    ListFuncionarioComponent,
    ModalFormFuncionarioComponent,
    ModalDetailFuncionarioComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ShowErrorsModule,
    FuncionarioRoutingModule,
    NgxSmartModalModule.forRoot()
  ]
})
export class FuncionarioModule { }
