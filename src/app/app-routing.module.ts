import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErroNotFoundComponent } from './erro-not-found/erro-not-found.component';

/**
 * Configurações de rotas do componente 'Principal' da aplicação.
 *
 * @author Gabrielllns
 */
export const AppRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: '**',
    component: ErroNotFoundComponent
  }
];

/**
 * Módulo responsável pelas configurações das rotas do módulo 'Principal' da aplicação.
 *
 * @author Gabrielllns
 */
@NgModule({
  imports: [RouterModule.forRoot(AppRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }