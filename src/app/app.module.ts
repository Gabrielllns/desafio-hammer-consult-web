import { ToastrModule } from 'ngx-toastr';
import { RouterModule } from '@angular/router';
import localePt from '@angular/common/locales/pt';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxUiLoaderModule, NgxUiLoaderConfig } from 'ngx-ui-loader';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { ErroNotFoundComponent } from './erro-not-found/erro-not-found.component';

import { HttpConfigInterceptor } from './config/interceptors/http-config.interceptor';

registerLocaleData(localePt);

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  "fgsColor": "#0769AD",
  "fgsSize": 70,
  "fgsType": "three-strings",
  "overlayColor": "rgba(0, 0, 0, 0.9)",
  "pbColor": "#0769AD",
  "hasProgressBar": true,
  "maxTime": -1,
  "minTime": 300
};

@NgModule({
  declarations: [
    AppComponent,
    ErroNotFoundComponent
  ],
  imports: [
    NgbModule,
    FormsModule,
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: "pt-BR"
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
