/**
 * Mapeia as constantes da aplicação.
 */
export class Constants {

    /**
     * Constantes de Perfil
     */
    public static readonly PERFIL_USUARIO: any = {
        ID_GERENTE: 1,
        ID_FUNCIONARIO: 2,
        ID_CONVIDADO: 3
    };

    /**
     * Valida se o perfil informado é 'gerente' ou 'funcionário'.
     *
     * @param idPerfil
     */
    public static isPerfilEmpresa(idPerfil: number): boolean {
        return (idPerfil === Constants.PERFIL_USUARIO.ID_GERENTE || idPerfil === Constants.PERFIL_USUARIO.ID_FUNCIONARIO);
    }

    /**
     * Valida se o perfil informado é 'convidado'.
     *
     * @param idPerfil
     */
    public static isPerfilConvidado(idPerfil: number): boolean {
        return (idPerfil === Constants.PERFIL_USUARIO.ID_CONVIDADO);
    }

    /**
     * Constantes de Title
     */
    public static readonly TITLE: any = {
        CONFIRMACAO: 'Confirmação',
    };

    /**
     * Constantes de Labels
     */
    public static readonly LABEL: any = {
        CONVIDADO: 'convidado(a)',
        FUNCIONARIO: 'funcionário(a)',
        NENHUM_RESULTADO_ENCONTRADO: 'Nenhum resultado encontrado',
    };

    /**
     * Constantes de Mensagens
     */
    public static readonly MSG: any = {
        DADOS_ALTERADOS: 'Dados alterados!',
        CONFIRMACAO_PARTICIPACAO_EVENTO: 'Deseja cancelar a participação do(a)',
        PARTICIPACAO_CONFIRMADA: 'Participação confirmada!',
        PARTICIPACOES_CONFIRMADAS: 'Participações confirmadas!',
        CADASTRO_REALIZADO: 'Cadastro realizado!',
        ACESSO_NAO_PERMITIDO: 'Acesso não permitido!',
        PARTICIPACAO_CANCELADA: 'Participação cancelada!',
    };

    /**
     * Constantes de Referências dos Loaders da Aplicação
     */
    public static readonly TASKID: any = {
        FORM_CONFIRMAR_PARTICIPACAO: 'form-confirmar-participacao',
        CANCELAR_PARTICIPACAO: 'cancelar-participacao',
        FORM_EVENTO: 'form-evento',
        LIST_EVENTO: 'list-evento',
        FORM_FUNCIONARIO: 'form-funcionario',
        LIST_FUNCIONARIO: 'list-funcionario',
    };

}
