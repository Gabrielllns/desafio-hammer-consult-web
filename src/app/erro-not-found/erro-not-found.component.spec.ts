import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErroNotFoundComponent } from './erro-not-found.component';

describe('ErroNotFoundComponent', () => {
  let component: ErroNotFoundComponent;
  let fixture: ComponentFixture<ErroNotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErroNotFoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErroNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
