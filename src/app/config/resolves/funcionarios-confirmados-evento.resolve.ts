import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Funcionario } from '../interfaces/funcionario';
import { FuncionarioService } from '../services/funcionario.service';

/**
 * Classe resolve responsável por retornar os 'Funionários' confirmados no 'Evento' por meio do 'id' do 'Evento' informado.
 *
 * @author Gabrielllns
 */
@Injectable({
  providedIn: 'root'
})
export class FuncionariosConfirmadosEventoResolve implements Resolve<any> {

  /**
   * Construtor da classe.
   *
   * @param funcionarioService
   * @param toastrService
   */
  constructor(private funcionarioService: FuncionarioService, private toastrService: ToastrService) { }

  /**
   * Resolve responsável por retornar os 'Funionários' confirmados no 'Evento' por meio do 'id' do 'Evento' informado.
   *
   * @param activatedRouteSnapshot
   */
  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<any> {
    let idEvento = Number(activatedRouteSnapshot.params['id']);

    return new Observable(observer => {

      this.funcionarioService.getFuncionariosConfirmadosPorEvento(idEvento).subscribe((funcionariosConfirmados: Funcionario[]) => {
        observer.next(funcionariosConfirmados);
        observer.complete();
      }, error => {
        observer.error(error);
        this.toastrService.error(error);
      });
    });
  }

}