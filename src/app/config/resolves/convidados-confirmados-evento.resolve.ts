import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Convidado } from '../interfaces/convidado';
import { ConvidadoService } from '../services/convidado.service';

/**
 * Classe resolve responsável por retornar os 'Convidados' confirmados no 'Evento' por meio do 'id' do 'Evento' informado.
 *
 * @author Gabrielllns
 */
@Injectable({
  providedIn: 'root'
})
export class ConvidadosConfirmadosEventoResolve implements Resolve<any> {

  /**
   * Construtor da classe.
   *
   * @param convidadoService
   * @param toastrService
   */
  constructor(private convidadoService: ConvidadoService, private toastrService: ToastrService) { }

  /**
   * Resolve responsável por retornar os 'Convidados' confirmados no 'Evento' por meio do 'id' do 'Evento' informado.
   *
   * @param activatedRouteSnapshot
   */
  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<any> {
    let idEvento = Number(activatedRouteSnapshot.params['id']);

    return new Observable(observer => {

      this.convidadoService.getConvidadosConfirmadosPorEvento(idEvento).subscribe((convidadosConfirmados: Convidado[]) => {
        observer.next(convidadosConfirmados);
        observer.complete();
      }, error => {
        observer.error(error);
        this.toastrService.error(error);
      });
    });
  }

}