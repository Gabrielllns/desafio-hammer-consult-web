import { Observable } from 'rxjs';
import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { Evento } from '../interfaces/evento';
import { EventoService } from '../services/evento.service';

/**
 * Classe resolve responsável por retornar todos os 'Eventos'.
 *
 * @author Gabrielllns
 */
@Injectable({
  providedIn: 'root'
})
export class EventosResolve implements Resolve<any> {

  /**
   * Construtor da classe.
   *
   * @param eventoService
   * @param toastrService
   */
  constructor(private eventoService: EventoService, private toastrService: ToastrService) { }

  /**
   * Resolve responsável por retornar todos os 'Eventos'.
   */
  resolve(): Observable<any> {

    return new Observable(observer => {

      this.eventoService.getEventos().subscribe((eventos: Evento[]) => {
        observer.next(eventos);
        observer.complete();
      }, error => {
        observer.error(error);
        this.toastrService.error(error);
      });
    });
  }

}