import { Observable } from 'rxjs';
import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { Funcionario } from '../interfaces/funcionario';
import { FuncionarioService } from '../services/funcionario.service';

/**
 * Classe resolve responsável por retornar todos os 'Funcionarios'.
 *
 * @author Gabrielllns
 */
@Injectable({
  providedIn: 'root'
})
export class FuncionariosResolve implements Resolve<any> {

  /**
   * Construtor da classe.
   *
   * @param funcionarioService
   * @param toastrService
   */
  constructor(private funcionarioService: FuncionarioService, private toastrService: ToastrService) { }

  /**
   * Resolve responsável por retornar todos os 'Funcionarios'.
   */
  resolve(): Observable<any> {

    return new Observable(observer => {

      this.funcionarioService.getFuncionarios().subscribe((funcionarios: Funcionario[]) => {
        observer.next(funcionarios);
        observer.complete();
      }, error => {
        observer.error(error);
        this.toastrService.error(error);
      });
    });
  }

}