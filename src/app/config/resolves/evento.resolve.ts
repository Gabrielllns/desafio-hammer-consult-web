import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Evento } from '../interfaces/evento';
import { EventoService } from '../services/evento.service';

/**
 * Classe resolve responsável por retornar o 'Evento' por meio do 'id' informado.
 *
 * @author Gabrielllns
 */
@Injectable({
  providedIn: 'root'
})
export class EventoResolve implements Resolve<any> {

  /**
   * Construtor da classe.
   *
   * @param eventoService
   * @param toastrService
   */
  constructor(private eventoService: EventoService, private toastrService: ToastrService) { }

  /**
   * Resolve responsável por retornar o 'Evento' por meio do 'id' informado.
   *
   * @param activatedRouteSnapshot
   */
  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<any> {
    let idEvento = Number(activatedRouteSnapshot.params['id']);

    return new Observable(observer => {

      this.eventoService.getEventoPorId(idEvento).subscribe((evento: Evento) => {
        observer.next(evento);
        observer.complete();
      }, error => {
        observer.error(error);
        this.toastrService.error(error);
      });
    });
  }

}