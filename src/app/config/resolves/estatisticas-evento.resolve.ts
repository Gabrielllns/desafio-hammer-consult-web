import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { EstatisticaEvento } from '../interfaces/estatistica-evento';
import { PresencaEventoService } from '../services/presenca-evento.service';

/**
 * Classe resolve responsável por retornar as 'Estatísticas' do 'Evento' informado.
 *
 * @author Gabrielllns
 */
@Injectable({
  providedIn: 'root'
})
export class EstatisticasEventoResolve implements Resolve<any> {

  /**
   * Construtor da classe.
   *
   * @param presencaEventoService
   * @param toastrService
   */
  constructor(private presencaEventoService: PresencaEventoService, private toastrService: ToastrService) { }

  /**
   * Resolve responsável por retornar as 'Estatísticas' do 'Evento' informado.
   *
   * @param activatedRouteSnapshot
   */
  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<any> {
    let idEvento = Number(activatedRouteSnapshot.params['id']);

    return new Observable(observer => {

      this.presencaEventoService.getEstatisticasEvento(idEvento).subscribe((estatisticasEvento: EstatisticaEvento) => {
        observer.next(estatisticasEvento);
        observer.complete();
      }, error => {
        observer.error(error);
        this.toastrService.error(error);
      });
    });
  }

}