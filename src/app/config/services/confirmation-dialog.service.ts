import { Injectable } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

import { ConfirmationDialogComponent } from '../components/confirmation-dialog/confirmation-dialog.component';

/**
 * Classe service responsável pelos 'Modais de Confirmação'.
 *
 * @author Tilo Westermann
 * @author Gabrielllns
 */
@Injectable({
  providedIn: 'root'
})
export class ConfirmationDialogService {

  /**
   * Construtor do service.
   *
   * @param ngbModalConfig
   * @param ngbModal
   */
  constructor(private ngbModalConfig: NgbModalConfig, private ngbModal: NgbModal) {
    this.ngbModalConfig.keyboard = false;
    this.ngbModalConfig.backdrop = 'static';
  }

  /**
   * Chama o modal de confirmação com os parâmetros recebidos.
   *
   * @param title
   * @param message
   */
  public confirm(title: string, message: string, sizeModal: string = 'lg'): Promise<boolean> {
    const modalRef = this.ngbModal.open(ConfirmationDialogComponent, { size: sizeModal });

    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;

    return modalRef.result;
  }

}
