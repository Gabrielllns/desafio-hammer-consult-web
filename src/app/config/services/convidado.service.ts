import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Convidado } from '../interfaces/convidado';
import { AbstractService } from './abstract.service';

/**
 * Classe service responsável pelos 'Convidados'.
 *
 * @author Gabrielllns
 */
@Injectable({
    providedIn: 'root'
})
export class ConvidadoService extends AbstractService {

    /**
     * Construtor do service.
     *
     * @param httpClient
     */
    constructor(private httpClient: HttpClient) {
        super('convidados');
    }

    /**
     * Retorna o convidado cadastrado por meio do 'id' informado.
     *
     * @param id
     */
    public getConvidadoPorId(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/' + id), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna todos os convidados cadastrados.
     */
    public getConvidados(): Observable<any> {
        return this.httpClient.get(this.getUrl(''), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna a lista de convidados confirmados conforme o 'id' do evento informado.
     *
     * @param id
     */
    public getConvidadosConfirmadosPorEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/confirmados/evento/' + id), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna o total de convidados confirmados conforme o 'id' do evento informado.
     *
     * @param id
     */
    public getTotalConvidadosConfirmadosPorEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/confirmados/evento/' + id + '/total'), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Salva os dados de convidado.
     *
     * @param convidado
     */
    public salvar(convidado: Convidado): Observable<any> {
        return this.httpClient.post(this.getUrl(''), { data: convidado }, this.httpOptions).pipe(catchError(this.resolveError));
    }

}