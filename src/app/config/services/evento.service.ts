import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Evento } from '../interfaces/evento';
import { AbstractService } from './abstract.service';

/**
 * Classe service responsável pelos 'Eventos'.
 *
 * @author Gabrielllns
 */
@Injectable({
    providedIn: 'root'
})
export class EventoService extends AbstractService {

    /**
     * Construtor do service.
     *
     * @param httpClient
     */
    constructor(private httpClient: HttpClient) {
        super('eventos');
    }

    /**
     * Retorna o evento cadastrado por meio do 'id' informado.
     *
     * @param id
     */
    public getEventoPorId(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/' + id), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna todos os eventos cadastrados.
     */
    public getEventos(): Observable<any> {
        return this.httpClient.get(this.getUrl(''), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Salva os dados de evento.
     *
     * @param evento
     */
    public salvar(evento: Evento): Observable<any> {
        return this.httpClient.post(this.getUrl(''), { data: evento }, this.httpOptions).pipe(catchError(this.resolveError));
    }

}