import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { AbstractService } from './abstract.service';

/**
 * Classe service responsável pelos 'Perfis'.
 *
 * @author Gabrielllns
 */
@Injectable({
    providedIn: 'root'
})
export class PerfilService extends AbstractService {

    /**
     * Construtor do service.
     *
     * @param httpClient
     */
    constructor(private httpClient: HttpClient) {
        super('perfis');
    }

    /**
     * Retorna o perfil cadastrado por meio do 'id' informado.
     *
     * @param id
     */
    public getPerfilPorId(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/' + id), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna todos os perfis cadastrados.
     */
    public getPerfis(): Observable<any> {
        return this.httpClient.get(this.getUrl(''), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna todos os perfis vinculados a empresa cadastrados.
     */
    public getPerfisEmpresa(): Observable<any> {
        return this.httpClient.get(this.getUrl('/empresa'), this.httpOptions).pipe(catchError(this.resolveError));
    }

}