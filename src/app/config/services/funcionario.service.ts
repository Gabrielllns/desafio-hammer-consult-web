import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { AbstractService } from './abstract.service';
import { Funcionario } from '../interfaces/funcionario';

/**
 * Classe service responsável pelos 'Funcionários'.
 *
 * @author Gabrielllns
 */
@Injectable({
    providedIn: 'root'
})
export class FuncionarioService extends AbstractService {

    /**
     * Construtor do service.
     *
     * @param httpClient
     */
    constructor(private httpClient: HttpClient) {
        super('funcionarios');
    }

    /**
     * Retorna o funcionário cadastrado por meio do 'id' informado.
     *
     * @param id
     */
    public getFuncionarioPorId(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/' + id), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna todos os funcionários cadastrados.
     */
    public getFuncionarios(): Observable<any> {
        return this.httpClient.get(this.getUrl(''), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna a lista de funcionários que não confirmaram presença no evento informado.
     *
     * @param id
     */
    public getFuncionarioPendentesPorEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/pendentes/evento/' + id), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna a lista de funcionários confirmados conforme o 'id' do evento informado.
     *
     * @param id
     */
    public getFuncionariosConfirmadosPorEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/confirmados/evento/' + id), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna o total de funcionários confirmados conforme o 'id' do evento informado.
     *
     * @param id
     */
    public getTotalFuncionariosConfirmadosPorEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/confirmados/evento/' + id + '/total'), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Salva os dados de funcionário.
     *
     * @param funcionario
     */
    public salvar(funcionario: Funcionario): Observable<any> {
        return this.httpClient.post(this.getUrl(''), { data: funcionario }, this.httpOptions).pipe(catchError(this.resolveError));
    }

}