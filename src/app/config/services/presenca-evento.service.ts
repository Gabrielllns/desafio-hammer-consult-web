import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { AbstractService } from './abstract.service';
import { PresencasEvento } from '../interfaces/presenca-evento';

/**
 * Classe service responsável pela 'Presença Evento'.
 *
 * @author Gabrielllns
 */
@Injectable({
    providedIn: 'root'
})
export class PresencaEventoService extends AbstractService {

    /**
     * Construtor do service.
     *
     * @param httpClient
     */
    constructor(private httpClient: HttpClient) {
        super('presencas-eventos');
    }

    /**
     * Cancela a participação do usuário no evento.
     *
     * @param id
     */
    public cancelar(id: number): Observable<any> {
        return this.httpClient.put(this.getUrl('/' + id + '/cancelar'), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Salva a participação de um funcionário/convidado no evento.
     *
     * @param presencasEvento
     */
    public salvar(presencasEvento: PresencasEvento): Observable<any> {
        return this.httpClient.post(this.getUrl(''), { data: presencasEvento }, this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna as estatísticas do evento informado.
     *
     * @param id
     */
    public getEstatisticasEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/evento/' + id + '/estatistica'), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna o total gasto com comida/bebida no evento informado.
     *
     * @param id
     */
    public getTotalGastoEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/evento/' + id + '/total-gasto'), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna o total gasto com comida no evento informado.
     *
     * @param id
     */
    public getTotalGastoComidaEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/evento/' + id + '/total-gasto/comida'), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna o total gasto com bebida no evento informado.
     *
     * @param id
     */
    public getTotalGastoBebidaEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/evento/' + id + '/total-gasto/bebida'), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna o total de funcionários/convidados confirmados no evento informado.
     *
     * @param id
     */
    public getTotalConvidadosEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/evento/' + id + '/total-participantes'), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna o total de convidados confirmados no evento informado.
     *
     * @param id
     */
    public getTotalConvidadosConfirmadosEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/evento/' + id + '/total-participantes/convidado'), this.httpOptions).pipe(catchError(this.resolveError));
    }

    /**
     * Retorna o total de funcionários confirmados no evento informado.
     *
     * @param id
     */
    public getTotalFuncionariosConfirmadosEvento(id: number): Observable<any> {
        return this.httpClient.get(this.getUrl('/evento/' + id + '/total-participantes/funcionario'), this.httpOptions).pipe(catchError(this.resolveError));
    }

}