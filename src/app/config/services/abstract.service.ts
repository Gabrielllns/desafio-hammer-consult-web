import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { environment } from '../../../environments/environment';

/**
 * Classe service abstrata.
 *
 * @author Gabrielllns
 */
export abstract class AbstractService {

  private baseUrl: string;

  protected httpOptions = {

    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
  };

  /**
   * Consturtor da classe.
   *
   * @param baseUrl
   */
  public constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  /**
   * Retorna a url absoluta considerando o 'contexto'  e a 'url relativa'.
   *
   * @param relativeUrl
   * @returns string
   */
  protected getUrl(relativeUrl?: string): string {
    let absoluteUrl = environment.url + this.baseUrl;

    if (relativeUrl !== null && relativeUrl !== undefined) {
      absoluteUrl += relativeUrl;
    }

    return absoluteUrl;
  }

  /**
   * Recupera o objeto 'Json' disponível no 'body' da 'Response'.
   *
   * @param response
   */
  protected resolveJsonData(response: Response) {
    return response.json();
  }

  /**
   * Recupera a 'mensagem de erro' disponível no 'body' da 'Response'.
   *
   * @param response
   */
  protected resolveError(response: any): Observable<any> {
    let text = undefined;

    if (response instanceof HttpErrorResponse) {

      if (response.status === 403 || response.status === 401) {
        text = undefined;
      } else {
        text = response.status === 0 ? 'Erro de comunicação!' : response.error;
      }
    } else {
      text = response.toString();
    }

    return throwError(text);
  }

}