import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';

import { Constants } from '../../constants';
import { Evento } from '../interfaces/evento';
import { EventoService } from '../services/evento.service';

/**
 * Class 'Guard' responsável por garantir que não ocorra acessos indevidos nas telas de 'Evento'.
 *
 * @author Gabrielllns
 */
@Injectable({
  providedIn: 'root'
})
export class EventoGuard implements CanActivate {

  /**
   * Construtor da classe.
   *
   * @param router
   * @param eventoService
   * @param toastrService
   */
  constructor(private router: Router, private eventoService: EventoService, private toastrService: ToastrService) { }

  /**
   * Verifica se o 'evento' em questão esta 'ativo'.
   *
   * @param route
   */
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let idEvento = Number(route.params['id']);

    return new Observable(observer => {

      this.eventoService.getEventoPorId(idEvento).subscribe((evento: Evento) => {

        if (!evento.st_ativo) {
          this.toastrService.error(Constants.MSG.ACESSO_NAO_PERMITIDO);
          this.router.navigate(['/']);
        }

        observer.next(evento.st_ativo);
      }, (error) => {
        this.toastrService.error(error);
        this.router.navigate(['/']);

        observer.next(false);
      }, () => {
        observer.complete();
      });
    });
  }
}
