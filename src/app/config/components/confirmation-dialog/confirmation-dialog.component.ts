import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

/**
 * Componente responsável pela estilização dos 'Modais de Confirmação'.
 *
 * @author Tilo Westermann
 * @author Gabrielllns
 */
@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent {

  @Input() title: string;
  @Input() message: string;
  @Input() btnOkText: string = 'Sim';
  @Input() btnCancelText: string = 'Não';
  @Input() btnOkClass: string = 'btn-danger';
  @Input() btnCancelClass: string = 'btn-success';

  /**
   * Construtor do componente.
   *
   * @param ngbActiveModal
   */
  constructor(private ngbActiveModal: NgbActiveModal) { }

  /**
   * Caso o usuário escolha a opção 'Não'.
   */
  public decline(): void {
    this.ngbActiveModal.close(false);
  }

  /**
   * Caso o usuário escolha a opção 'Sim'.
   */
  public accept(): void {
    this.ngbActiveModal.close(true);
  }

  /**
   * Caso o usuário escolha a opção 'Fechar'.
   */
  public dismiss(): void {
    this.ngbActiveModal.dismiss();
  }

}
