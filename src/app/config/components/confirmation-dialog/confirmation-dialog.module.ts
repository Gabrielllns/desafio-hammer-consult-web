import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfirmationDialogComponent } from './confirmation-dialog.component';

/**
 * Módulo de controle responsável pelo componente de 'ConfirmationDialogComponent'.
 *
 * @author Gabrielllns
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ConfirmationDialogComponent
  ],
  exports: [
    ConfirmationDialogComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ]
})
export class ConfirmationDialogModule { }
