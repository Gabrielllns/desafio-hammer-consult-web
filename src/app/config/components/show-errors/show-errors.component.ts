import { Component, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl, NgForm } from '@angular/forms';

/**
 * Componente responsável pela estilização dos 'error' nos formulários.
 *
 * @author Igor Geshoski
 * @author Gabrielllns
 */
@Component({
  selector: 'show-errors',
  templateUrl: './show-errors.component.html',
  styleUrls: ['./show-errors.component.scss']
})
export class ShowErrorsComponent {

  private static readonly errorMessages = {
    'bsDate': () => 'Campo obrigatório.',
    'required': () => 'Campo obrigatório.',
    'email': () => 'Informe um e-mail válido.',
    'minlength': (params) => 'Favor informar no mínimo ' + params.requiredLength + ' caracteres.',
    'maxlength': (params) => 'A máximo de caracteres permitidos são ' + params.requiredLength + '.',
  };

  private static readonly errorMessagesRadioCheck = {
    'required': () => 'Informe um item.',
  };

  @Input() private ngForm: NgForm;
  @Input() private isRadioOrCheck: boolean = false;
  @Input() private control: AbstractControlDirective | AbstractControl;

  /**
   * Responsável pelas validações dos erros.
   */
  public shouldShowErrors(): boolean {
    return ((this.control && this.control.errors) && (!this.ngForm.valid && this.ngForm.submitted));
  }

  /**
   * Responsável pela listagem de erros.
   */
  public listOfErrors(): string[] {
    return Object.keys(this.control.errors).map(field => this.getMessage(field, this.control.errors[field]));
  }

  /**
   * Retorna a mensagem conforme os parâmetros recebidos.
   *
   * @param type
   * @param params
   */
  private getMessage(type: string, params: any): any {
    let message = undefined;

    if (this.isRadioOrCheck) {
      message = ShowErrorsComponent.errorMessagesRadioCheck[type](params);
    } else {
      message = ShowErrorsComponent.errorMessages[type](params);
    }

    return message;
  }
}