import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowErrorsComponent } from './show-errors.component';

/**
 * Módulo de controle responsável pelo componente de 'ShowErrors'.
 *
 * @author Gabrielllns
 */
@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    ShowErrorsComponent
  ],
  exports: [
    ShowErrorsComponent
  ]
})
export class ShowErrorsModule { }
