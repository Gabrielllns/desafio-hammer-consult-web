import { Component, Input } from '@angular/core';

/**
 * Componente responsável pela customização do 'card-event'.
 *
 * @author Gabrielllns
 */
@Component({
  selector: 'card-event',
  templateUrl: './card-event.component.html',
  styleUrls: ['./card-event.component.scss']
})
export class CardEventComponent {

  @Input() public titulo: string;
  @Input() public idEvento: number;
  @Input() public infoCriacao: string;
  @Input() public statusEvento: boolean;
  @Input() public infoAlteracao: string;
  @Input() public dataRealizacao: string;

  /**
   * Construtor do componente.
   */
  constructor() { }

}
