import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CardEventComponent } from './card-event.component';

/**
 * Módulo de controle responsável pelo componente de 'CardEvent'.
 *
 * @author Gabrielllns
 */
@NgModule({
  imports: [
    RouterModule,
    CommonModule,
  ],
  declarations: [
    CardEventComponent
  ],
  exports: [
    CardEventComponent
  ]
})
export class CardEventModule { }
