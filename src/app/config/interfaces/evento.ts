import { Usuario } from './usuario';

/**
 * Interface de controle responsável por mapear os atributos de 'Evento'.
 *
 * @author Gabrielllns
 */
export interface Evento {
    id?: number
    titulo?: string
    descricao?: string
    data_realizacao?: string
    horario_realizacao?: string
    data_realizacaoAUX?: Date
    horario_realizacaoAUX?: Date
    id_usuario_responsavel?: number
    st_ativo?: boolean
    usuario?: Usuario
}
