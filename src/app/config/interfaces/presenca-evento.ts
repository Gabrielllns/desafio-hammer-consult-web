import { Evento } from './evento';
import { Usuario } from './usuario';
import { Convidado } from './convidado';
import { Funcionario } from './funcionario';

/**
 * Interface de controle responsável por mapear os atributos de 'PresençaEvento'.
 *
 * @author Gabrielllns
 */
export interface PresencaEvento {
    id?: number
    id_evento?: number
    id_usuario?: number
    st_bebe?: boolean
    usuario?: Usuario
    evento?: Evento
}

/**
 * Interface de controle responsável por mapear os atributos de 'PresencasEvento'.
 *
 * @author Gabrielllns
 */
export interface PresencasEvento {
    has_convidado?: boolean
    id_evento?: number
    funcionario?: PresencaEvento
    convidado?: PresencaEvento
}
