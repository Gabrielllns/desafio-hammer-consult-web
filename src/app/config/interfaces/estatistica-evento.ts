/**
 * Interface de controle responsável por mapear os atributos de 'EstatisticaEvento'.
 *
 * @author Gabrielllns
 */
export interface EstatisticaEvento {
    totalFuncionariosConfirmados?: number
    totalConvidadosConfirmados?: number
    totalArrecadado?: number
    totalGasto?: number
    totalGastoComida?: number
    totalGastoBebida?: number
}
