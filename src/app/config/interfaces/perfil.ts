/**
 * Interface de controle responsável por mapear os atributos de 'Perfil'.
 *
 * @author Gabrielllns
 */
export interface Perfil {
    id?: number
    descricao?: string
    st_ativo?: boolean
    st_empresa?: boolean
}
