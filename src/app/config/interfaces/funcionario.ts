import { Usuario } from './usuario';
import { Convidado } from './convidado';

/**
 * Interface de controle responsável por mapear os atributos de 'Funcionário'.
 *
 * @author Gabrielllns
 */
export interface Funcionario {
    id?: number
    id_usuario?: number
    codigo?: number
    usuario?: Usuario
    convidado?: Convidado
    created_at?: Date
    updated_at?: Date
}

/**
 * Interface de controle responsável por mapear os atributos de 'Funcionário' no componente 'select'.
 *
 * @author Gabrielllns
 */
export interface SelectFuncionario {
    id?: number
    nome?: string
}
