import { Usuario } from './usuario';
import { Funcionario } from './funcionario';

/**
 * Interface de controle responsável por mapear os atributos de 'Convidado'.
 *
 * @author Gabrielllns
 */
export interface Convidado {
    id?: number
    id_usuario?: number
    id_funcionario_responsavel?: number
    usuario?: Usuario
    funcionario_responsavel?: Funcionario
}
