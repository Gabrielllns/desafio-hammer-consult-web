import { Funcionario } from './funcionario';
import { Perfil } from './perfil';
import { Convidado } from './convidado';
import { PresencaEvento } from './presenca-evento';

/**
 * Interface de controle responsável por mapear os atributos de 'Usuário'.
 *
 * @author Gabrielllns
 */
export interface Usuario {
    id?: number
    nome?: string
    id_perfil?: number
    st_ativo?: boolean
    funcionario?: Funcionario
    convidado?: Convidado
    perfil?: Perfil
    presenca_evento?: PresencaEvento
}
