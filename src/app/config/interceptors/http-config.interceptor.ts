import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

/**
 * Interceptor criado para interceptar requisições feitas a API.
 *
 * @author Gabrielllns
 */
@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

  protected activeRequests: number = 0;

  /**
   * URLs que não habilitam o load.
   */
  protected skippUrls = ['/'];

  /**
   * Construtor do interceptor.
   *
   * @param ngxUiLoaderService
   */
  constructor(private ngxUiLoaderService: NgxUiLoaderService) { }

  /**
   * Função de interceptação das requisições.
   *
   * @param request
   * @param next
   */
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.ngxUiLoaderService.start();

    if (this.activeRequests === 0) {
      this.ngxUiLoaderService.start();
    }

    this.activeRequests++;

    return next.handle(request).pipe(finalize(() => {
      this.activeRequests--;

      if (this.activeRequests === 0) {
        this.ngxUiLoaderService.stop();
      }
    }));
  };
}