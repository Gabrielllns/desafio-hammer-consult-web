import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxSelectModule } from 'ngx-select-ex';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { ListEventoComponent } from './list-evento/list-evento.component';
import { FormEventoComponent } from './form-evento/form-evento.component';
import { DetailEventoComponent } from './detail-evento/detail-evento.component';

import { EventoRoutingModule } from './evento-routing.module';
import { CardEventModule } from '../config/components/card-event/card-event.module';
import { ShowErrorsModule } from '../config/components/show-errors/show-errors.module';
import { ModalConfirmarParticipacaoComponent } from './modal-confirmar-participacao/modal-confirmar-participacao.component';

defineLocale('pt-br', ptBrLocale);

@NgModule({
  declarations: [
    ListEventoComponent,
    FormEventoComponent,
    DetailEventoComponent,
    ModalConfirmarParticipacaoComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    CardEventModule,
    NgxSelectModule,
    ShowErrorsModule,
    EventoRoutingModule,
    AccordionModule.forRoot(),
    TimepickerModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxSmartModalModule.forRoot()
  ]
})
export class EventoModule { }
