import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventoGuard } from '../config/guards/evento.guard';

import { ListEventoComponent } from './list-evento/list-evento.component';
import { FormEventoComponent } from './form-evento/form-evento.component';
import { DetailEventoComponent } from './detail-evento/detail-evento.component';

import { EventoResolve } from '../config/resolves/evento.resolve';
import { EventosResolve } from '../config/resolves/eventos.resolve';
import { EstatisticasEventoResolve } from '../config/resolves/estatisticas-evento.resolve';
import { ConvidadosConfirmadosEventoResolve } from '../config/resolves/convidados-confirmados-evento.resolve';
import { FuncionariosConfirmadosEventoResolve } from '../config/resolves/funcionarios-confirmados-evento.resolve';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: 'list',
    component: ListEventoComponent,
    resolve: {
      eventos: EventosResolve
    }
  },
  {
    path: ':id/detail',
    component: DetailEventoComponent,
    resolve: {
      evento: EventoResolve,
      estatisticasEvento: EstatisticasEventoResolve,
      convidadosConfirmadosEvento: ConvidadosConfirmadosEventoResolve,
      funcionariosConfirmadosEvento: FuncionariosConfirmadosEventoResolve
    },
    canActivate: [
      EventoGuard
    ]
  },
  {
    path: 'add',
    component: FormEventoComponent,
  },
  {
    path: ':id/edit',
    component: FormEventoComponent,
    resolve: {
      evento: EventoResolve
    },
    canActivate: [
      EventoGuard
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventoRoutingModule { }
