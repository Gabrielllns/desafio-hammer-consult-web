import { isUndefined } from 'util';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Constants } from 'src/app/constants';
import { PresencasEvento } from 'src/app/config/interfaces/presenca-evento';
import { FuncionarioService } from 'src/app/config/services/funcionario.service';
import { SelectFuncionario, Funcionario } from 'src/app/config/interfaces/funcionario';
import { PresencaEventoService } from 'src/app/config/services/presenca-evento.service';

@Component({
  selector: 'app-modal-confirmar-participacao',
  templateUrl: './modal-confirmar-participacao.component.html',
  styleUrls: ['./modal-confirmar-participacao.component.scss']
})
export class ModalConfirmarParticipacaoComponent {

  public refModal: any;
  public funcionario: Funcionario = {};
  public funcionariosPendentesEvento: SelectFuncionario[] = [];
  public presencasEvento: PresencasEvento = { convidado: { usuario: {} }, funcionario: { usuario: {} } };

  @Input() idEvento: number;
  @Output() onSuccess: EventEmitter<boolean> = new EventEmitter(false);

  /**
   * Construtor do componente.
   *
   * @param funcionarioService
   * @param ngxSmartModalService
   * @param presencaEventoService
   * @param toastrService
   * @param ngxUiLoaderService
   */
  constructor(private funcionarioService: FuncionarioService, private ngxSmartModalService: NgxSmartModalService, private presencaEventoService: PresencaEventoService, private toastrService: ToastrService, private ngxUiLoaderService: NgxUiLoaderService) { }

  /**
   * Inicia as configurações iniciais do modal.
   *
   * @param refModal
   */
  public init(refModal: any): void {
    this.refModal = refModal;
    this.funcionario = this.refModal.getData();
    this.presencasEvento.id_evento = this.idEvento;

    if (isUndefined(this.funcionario)) {
      this.getFuncionarioPendentesPorEvento(this.idEvento);
    } else {
      this.presencasEvento.has_convidado = true;

      this.presencasEvento.funcionario = this.funcionario;
      this.presencasEvento.funcionario.st_bebe = this.funcionario.usuario.presenca_evento.st_bebe;
    }
  }

  /**
   *Retorna a lista de funcionários que não confirmaram presença no evento informado.
   *
   * @param idEvento
   */
  private getFuncionarioPendentesPorEvento(idEvento: number): void {

    this.funcionarioService.getFuncionarioPendentesPorEvento(idEvento).subscribe((funcionariosPendentesEvento: Funcionario[]) => {

      if (funcionariosPendentesEvento.length == 0) {
        this.funcionariosPendentesEvento.push({ id: undefined, nome: Constants.LABEL.NENHUM_RESULTADO_ENCONTRADO });
      } else {

        funcionariosPendentesEvento.forEach((funcionarioPendenteEvento: Funcionario) => {
          let selectFuncionario: SelectFuncionario = {
            id: funcionarioPendenteEvento.usuario.id,
            nome: funcionarioPendenteEvento.usuario.nome
          };

          this.funcionariosPendentesEvento.push(selectFuncionario);
        });
      }
    }, (error) => {
      this.refModal.close();
      this.toastrService.error(error);
    });
  }

  /**
   * Reseta as dependências do modal.
   *
   * @param ngForm
   */
  public resetDependencies(ngForm: NgForm): void {
    this.ngxSmartModalService.getModal(this.refModal.identifier).removeData();
    this.funcionariosPendentesEvento = [];
    ngForm.resetForm();
  }

  /**
   * Limpa a instância de convidado.
   */
  public limparConvidado(): void {
    this.presencasEvento.convidado = { usuario: {} };
  }

  /**
   * Retorna o vcalor total a ser pago no evento.
   *
   * @param presencasEvento
   */
  public getTotalValorPagamento(presencasEvento: PresencasEvento): number {
    let valorTotal = 0.00;

    valorTotal += (presencasEvento.funcionario.st_bebe) ? 20.00 : 10.00;

    if (presencasEvento.has_convidado) {
      valorTotal += (presencasEvento.convidado.st_bebe) ? 20.00 : 10.00;
    }

    return valorTotal;
  }

  /**
   * Salva o registro com base nos dados informados.
   *
   * @param ngForm
   * @param presencasEvento
   * @param modalFormFinance
   */
  public save(ngForm: NgForm, presencasEvento: PresencasEvento, modalFormFinance: any): void {

    if (ngForm.valid) {
      this.ngxUiLoaderService.start(Constants.TASKID.FORM_CONFIRMAR_PARTICIPACAO);

      this.presencaEventoService.salvar(presencasEvento).subscribe(() => {
        this.toastrService.success((!presencasEvento.has_convidado) ? Constants.MSG.PARTICIPACAO_CONFIRMADA : Constants.MSG.PARTICIPACOES_CONFIRMADAS);

        this.ngxUiLoaderService.stop(Constants.TASKID.FORM_CONFIRMAR_PARTICIPACAO);
        this.onSuccess.emit(presencasEvento.has_convidado);
        modalFormFinance.close();
      }, (error) => {
        this.toastrService.error(error);
        this.ngxUiLoaderService.stop(Constants.TASKID.FORM_CONFIRMAR_PARTICIPACAO);
      });
    }
  }

}
