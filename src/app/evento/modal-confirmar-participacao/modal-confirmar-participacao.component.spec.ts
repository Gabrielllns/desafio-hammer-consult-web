import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmarParticipacaoComponent } from './modal-confirmar-participacao.component';

describe('ModalConfirmarParticipacaoComponent', () => {
  let component: ModalConfirmarParticipacaoComponent;
  let fixture: ComponentFixture<ModalConfirmarParticipacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConfirmarParticipacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmarParticipacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
