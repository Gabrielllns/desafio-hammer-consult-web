import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NgxSmartModalService } from 'ngx-smart-modal';

import { Constants } from '../../constants';
import { Evento } from 'src/app/config/interfaces/evento';
import { EventoService } from 'src/app/config/services/evento.service';

@Component({
  selector: 'app-list-evento',
  templateUrl: './list-evento.component.html',
  styleUrls: ['./list-evento.component.scss']
})
export class ListEventoComponent {

  public eventos: Evento[] = [];

  /**
   * Construtor do componente.
   *
   * @param eventoService
   * @param toastrService
   * @param ngxUiLoaderService
   * @param ngxSmartModalService
   * @param activatedRoute
   */
  constructor(private eventoService: EventoService, private toastrService: ToastrService, private ngxUiLoaderService: NgxUiLoaderService, private ngxSmartModalService: NgxSmartModalService, private activatedRoute: ActivatedRoute) {
    this.eventos = this.activatedRoute.snapshot.data['eventos'];
  }

  /**
   * Abre o modal de evento.
   *
   * @param nomeModal
   * @param evento
   */
  public openModalEvento(nomeModal: string, evento?: Evento): void {
    this.ngxSmartModalService.setModalData(evento, nomeModal);
    this.ngxSmartModalService.getModal(nomeModal).open();
  }

  /**
   * Retorna todos os eventos cadastrados.
   */
  public getEventos(): void {
    this.ngxUiLoaderService.start(Constants.TASKID.LIST_EVENTO);

    this.eventoService.getEventos().subscribe((eventos: Evento[]) => {
      this.eventos = eventos;
      this.ngxUiLoaderService.stop(Constants.TASKID.LIST_EVENTO);
    }, (error) => {
      this.toastrService.error(error);
      this.ngxUiLoaderService.stop(Constants.TASKID.LIST_EVENTO);
    });
  }

}
