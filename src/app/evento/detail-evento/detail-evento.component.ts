import { isUndefined } from 'util';
import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { NgxSmartModalService } from 'ngx-smart-modal';

import { Constants } from '../../constants';
import { Usuario } from '../../config/interfaces/usuario';
import { Evento } from 'src/app/config/interfaces/evento';
import { Convidado } from 'src/app/config/interfaces/convidado';
import { Funcionario } from 'src/app/config/interfaces/funcionario';
import { ConvidadoService } from 'src/app/config/services/convidado.service';
import { EstatisticaEvento } from 'src/app/config/interfaces/estatistica-evento';
import { FuncionarioService } from 'src/app/config/services/funcionario.service';
import { PresencaEventoService } from '../../config/services/presenca-evento.service';
import { ConfirmationDialogService } from '../../config/services/confirmation-dialog.service';

@Component({
  selector: 'app-detail-evento',
  templateUrl: './detail-evento.component.html',
  styleUrls: ['./detail-evento.component.scss']
})
export class DetailEventoComponent {

  public evento: Evento = {};
  public estatisticasEvento: EstatisticaEvento = {};
  public convidadosConfirmadosEvento: Convidado[] = [];
  public funcionariosConfirmadosEvento: Funcionario[] = [];

  /**
   * Construtor do componente.
   *
   * @param presencaEventoService
   * @param convidadoService
   * @param funcionarioService
   * @param ngxSmartModalService
   * @param confirmationDialogService
   * @param toastrService
   * @param ngxUiLoaderService
   * @param activatedRoute
   */
  constructor(private presencaEventoService: PresencaEventoService, private convidadoService: ConvidadoService, private funcionarioService: FuncionarioService, private ngxSmartModalService: NgxSmartModalService, private confirmationDialogService: ConfirmationDialogService, private toastrService: ToastrService, private ngxUiLoaderService: NgxUiLoaderService, private activatedRoute: ActivatedRoute) {
    this.evento = this.activatedRoute.snapshot.data['evento'];
    this.estatisticasEvento = this.activatedRoute.snapshot.data['estatisticasEvento'];
    this.convidadosConfirmadosEvento = this.activatedRoute.snapshot.data['convidadosConfirmadosEvento'];
    this.funcionariosConfirmadosEvento = this.activatedRoute.snapshot.data['funcionariosConfirmadosEvento'];
  }

  /**
   * Abre o modal de confirmação de participação no evento.
   *
   * @param nomeModal
   * @param funcionario
   */
  public openModalParticipacaoEvento(nomeModal: string, funcionario?: Funcionario): void {
    this.ngxSmartModalService.setModalData(funcionario, nomeModal);
    this.ngxSmartModalService.getModal(nomeModal).open();
  }

  /**
   * Confirma o cancelamento da participação do convidado/funcionário.
   *
   * @param usuario
   * @param evento
   * @param accordionRef
   */
  public cancelarParticipacao(usuario: Usuario, evento: Evento, accordionRef: any): void {

    if (!isUndefined(usuario) && !isUndefined(usuario.id_perfil) && !isUndefined(evento)) {
      let tipoUsuario = (usuario.id_perfil != 3) ? Constants.LABEL.FUNCIONARIO : Constants.LABEL.CONVIDADO;

      this.confirmationDialogService.confirm(Constants.TITLE.CONFIRMACAO, `${Constants.MSG.CONFIRMACAO_PARTICIPACAO_EVENTO} ${tipoUsuario}: ${usuario.nome}?`).then((confirmed) => {

        if (confirmed) {
          accordionRef.groups.forEach((elemento) => { elemento.isOpen = false; });

          this.cancelar(usuario, evento);
        }
      });
    }
  }

  /**
   * Atualiza a lista de participantes.
   *
   * @param has_convidado
   * @param evento
   * @param accordionRef
   */
  public reloadParticipantes(has_convidado: boolean, evento: Evento, accordionRef: any): void {
    accordionRef.groups.forEach((elemento) => { elemento.isOpen = false; });

    this.ngxUiLoaderService.start(Constants.TASKID.CANCELAR_PARTICIPACAO);
    this.getEstatisticasEvento(has_convidado, evento);
  }

  /**
   * Retorna as estatísticas do evento informado.
   *
   * @param has_convidado
   * @param evento
   */
  private getEstatisticasEvento(has_convidado: boolean, evento: Evento): void {

    this.presencaEventoService.getEstatisticasEvento(evento.id).subscribe((estatisticasEvento: EstatisticaEvento) => {
      this.estatisticasEvento = estatisticasEvento;

      this.getFuncionariosConfirmadosPorEvento(evento, has_convidado);

      if (has_convidado) {
        this.getConvidadosConfirmadosPorEvento(evento);
      }
    }, (error) => {
      this.ngxUiLoaderService.stop(Constants.TASKID.CANCELAR_PARTICIPACAO);
      this.toastrService.error(error);
    });
  }

  /**
   * Cancela a participação do usuário no evento.
   *
   * @param usuario
   * @param evento
   */
  private cancelar(usuario: Usuario, evento: Evento): void {
    this.ngxUiLoaderService.start(Constants.TASKID.CANCELAR_PARTICIPACAO);

    this.presencaEventoService.cancelar(usuario.presenca_evento.id).subscribe(() => {
      this.toastrService.success(Constants.MSG.PARTICIPACAO_CANCELADA);

      this.getEstatisticasEvento(Constants.isPerfilConvidado(usuario.id_perfil), evento);
    }, (error) => {
      this.ngxUiLoaderService.stop(Constants.TASKID.CANCELAR_PARTICIPACAO);
      this.toastrService.error(error);
    });
  }

  /**
   * Retorna a lista de funcionários confirmados conforme o 'id' do evento informado.
   *
   * @param evento
   * @param has_convidado
   */
  private getFuncionariosConfirmadosPorEvento(evento: Evento, has_convidado: boolean = false): void {

    this.funcionarioService.getFuncionariosConfirmadosPorEvento(evento.id).subscribe((funcionariosConfirmadosEvento: Funcionario[]) => {
      this.funcionariosConfirmadosEvento = funcionariosConfirmadosEvento;

      if (!has_convidado) {
        this.ngxUiLoaderService.stop(Constants.TASKID.CANCELAR_PARTICIPACAO);
      }
    }, (error) => {
      this.ngxUiLoaderService.stop(Constants.TASKID.CANCELAR_PARTICIPACAO);
      this.toastrService.error(error);
    });
  }

  /**
   * Retorna a lista de convidados confirmados conforme o 'id' do evento informado.
   *
   * @param evento
   */
  private getConvidadosConfirmadosPorEvento(evento: Evento): void {

    this.convidadoService.getConvidadosConfirmadosPorEvento(evento.id).subscribe((convidadosConfirmadosEvento: Convidado[]) => {
      this.convidadosConfirmadosEvento = convidadosConfirmadosEvento;
      this.ngxUiLoaderService.stop(Constants.TASKID.CANCELAR_PARTICIPACAO);
    }, (error) => {
      this.ngxUiLoaderService.stop(Constants.TASKID.CANCELAR_PARTICIPACAO);
      this.toastrService.error(error);
    });
  }

}
