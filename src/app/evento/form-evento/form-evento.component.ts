import * as moment from 'moment';
import { isUndefined } from 'util';
import { NgForm } from '@angular/forms';
import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router, ActivatedRoute } from '@angular/router';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

import { Constants } from '../../constants';
import { Evento } from 'src/app/config/interfaces/evento';
import { EventoService } from 'src/app/config/services/evento.service';

@Component({
  selector: 'app-form-evento',
  templateUrl: './form-evento.component.html',
  styleUrls: ['./form-evento.component.scss']
})
export class FormEventoComponent {

  public evento: Evento = {};
  public minDate = new Date();

  /**
   * Construtor do componente.
   *
   * @param activatedRoute
   * @param router
   * @param bsLocaleService
   * @param toastrService
   * @param ngxUiLoaderService
   * @param eventoService
   */
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private bsLocaleService: BsLocaleService, private toastrService: ToastrService, private ngxUiLoaderService: NgxUiLoaderService, private eventoService: EventoService) {
    this.bsLocaleService.use('pt-br');

    if (this.activatedRoute.snapshot.data['evento']) {
      this.evento = this.activatedRoute.snapshot.data['evento'];
      this.evento.data_realizacaoAUX = this.evento.horario_realizacaoAUX = moment(this.evento.data_realizacao).add(3, 'hours').toDate();
    }
  }

  /**
   * Salva o registro com base nos dados informados.
   *
   * @param ngForm
   * @param evento
   */
  public save(ngForm: NgForm, evento: Evento): void {

    if (ngForm.valid) {
      this.ngxUiLoaderService.start(Constants.TASKID.FORM_EVENTO);

      evento.data_realizacao = moment(evento.data_realizacaoAUX).format(moment.HTML5_FMT.DATE).toString();
      evento.horario_realizacao = moment(evento.horario_realizacaoAUX).format(moment.HTML5_FMT.TIME).toString();

      this.eventoService.salvar(evento).subscribe(() => {
        this.router.navigate(['/evento/list']);

        this.toastrService.success((!isUndefined(evento.id)) ? Constants.MSG.DADOS_ALTERADOS : Constants.MSG.CADASTRO_REALIZADO);

        this.ngxUiLoaderService.stop(Constants.TASKID.FORM_EVENTO);
      }, (error) => {
        this.toastrService.error(error);
        this.ngxUiLoaderService.stop(Constants.TASKID.FORM_EVENTO);
      });
    }
  }

}
