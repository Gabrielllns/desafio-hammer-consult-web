# PROBLEMA #

Aqui na hammer organizamos um churras todos os meses, **é necessario que cada funcionario contribua 20$ e se quiser levar um convidado 40$**.
O problema é que não temos uma maneira para controlar quem irá no churrasco e quanto foi gasto com comida e bebida.
**Um funcionário pode levar apenas um convidado,se você não beber,o valor do churrasco será metade,a mesma regra é valida para o convidado.**
Dado o problema é necessario desenvolver uma aplicação WebApi para consumo do nosso front.

### Dependências: ###

* Node.js >= 12.15.x

### Tecnologias: ###

* **Front-end:** Gerado com [Angular CLI](https://github.com/angular/angular-cli) em sua versão 10.0.2.
* **Back-end:** Gerado com [Laravel Framework](https://github.com/laravel/laravel) em sua versão 7.x. 
* **Banco de Dados:** Mysql.

### Executando o projeto: ###

* Clonar o projeto;
* Entrar na pasta do projeto;
* Instalar as dependências através do comando `npm install`
* Executar o comando `ng serve --open`;
* Aguarde e será redirecionado para o link: `http://localhost:4200/`

## Informações ###

Para mais informações execute no terminal o comando `ng help` ou consulte [Angular CLI](https://github.com/angular/angular-cli/blob/master/README.md).
